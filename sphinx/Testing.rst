Testing
=======

divERGe uses Catch3 for testing. Running the tests can be achieved by a call to
:c:func:`diverge_run_tests`, or by running the ``divERGe`` executable.

.. c:autodoc:: diverge_run_tests.h
