License
=======

divERGe is licensed under `GPLv3 <https://www.gnu.org/licenses/gpl-3.0.html>`_.
The license can be obtained from within the code using the following functions:

.. c:autodoc:: misc/license.h
