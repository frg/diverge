.. _Examples:

Examples
========

Examples are found in the :release_tree:`examples/ <examples>` directory. In the
git repository, they are grouped into programming languages. In the release
tarball, all examples are merged within ``examples/``.

For step-by-step tutorials of the python interface, look at
:release_tree:`examples/*-?-*.py <examples/python_tutorial>`,
i.e., either the files

.. sourcecode:: bash

    wham-0-init.py
    wham-1-model-nickelate.py
    wham-2-plot.py
    wham-3-interactions.py
    wham-4-flow.py

for the tutorial presented at an internal workshop in 2023, or

.. sourcecode:: bash

    emergent-0-drosophila.py
    emergent-1-orbitals.py
    emergent-2-plot.py
    emergent-3-model-details.py
    emergent-4-symmetries.py
    emergent-5-npatch.py
    emergent-6-patchplot.py
    emergent-7-tufrg.py
    emergent-8-flow.py
    emergent-9-nonsu2.py

for the tutorial presented at EMERGENT in Sep, 2023.

The :ref:`C interface` is documented in a step-by-step fashion in the
:ref:`Tutorial` section, or by looking at the C/C++ examples. A good
starting point is :release_tree:`hubbard.c <examples/c_cpp/hubbard.c>`.
