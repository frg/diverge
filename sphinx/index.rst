divERGe implements various Exact Renormalization Group examples
===============================================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   Getting Started
   Compilation
   C Interface

.. toctree::
   :maxdepth: 2

   Examples
   Python Interface
   Simulation Output
   Runtime Tuning

Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`

