Initialization
==============

To intialize the library, a call to :c:func:`diverge_init` is required.
Correspondingly, the divERGe context must be cleaned up by calling
:c:func:`diverge_finalize`.

.. c:autodoc:: misc/mpi_functions.h

.. toctree::
   Allocators
   License
   Status
   Logging
   Datatypes
   Shared Memory

