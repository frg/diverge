.. _AutoflowSec:

Autoflow
========

"A feature that may seem appealing to newcomers, but is frowned upon by seasoned
users." -- a possible summary of the :func:`diverge.autoflow` function. Very
useful for rapid progress in getting to know the library or having a very quick
look at a model. Quite nice to build your own scripts on, but to do that you
have to dig for the source. Good entry point to see the library in action.

.. autofunction:: diverge.autoflow
   :no-index:
