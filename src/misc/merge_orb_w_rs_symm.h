/* Copyright (C) 2024 Jonas B. Profe, Lennart Klebl; GPLv3+ (LICENSE.md) */

#pragma once
#include "../diverge_model.h"
#include "../diverge_internals_struct.h"


#ifdef __cplusplus
extern "C" {
#endif

/** this function only is in this header to be in the public interface */
void merge_rs_orb(diverge_model_t* model);

#ifdef __cplusplus
}
#endif
