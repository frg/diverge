/* Copyright (C) 2024 Jonas B. Profe, Lennart Klebl; GPLv3+ (LICENSE.md) */

#pragma once

#include "../diverge_common.h"

#ifdef __cplusplus

#include <vector>
using std::vector;

extern "C" {
#endif

// forward declaration
typedef struct rs_hopping_t rs_hopping_t;

/**
 * reads Wannier90's \*_hr.dat file into divERGe's :c:struct:`rs_hopping_t` data
 * structure.
 *
 * :param const char* fname: /path/to/material_hr.dat
 * :param index_t n_spin: default value 0 amounts to :math:`SU(2)` symmetric model. if
 *                        :math:`n_\mathrm{spin} \neq 0`,
 *                        :math:`|n_\mathrm{spin}| = 2S+1` with :math:`S` the
 *                        physical spin (i.e., for :math:`S=1/2` we have
 *                        :math:`|n_\mathrm{spin}|=2`). the sign determines
 *                        whether the spin index is the one which increases
 *                        memory slowly (negative) or fast (positive) in the W90
 *                        file. Note that the divERGe convention is always
 *                        (s,o), i.e. spin indices increasing memory fast
 *                        ('outer indices').
 * :param index_t* len: output for the number of hoppings that have been read.
 * :param index_t* n_orb: output for the maximum number of orbitals that were
 *                        found in the hr.dat file. not considered if NULL. In
 *                        case ``n_orb != NULL`` and ``~(*n_orb) == 0``, swap
 *                        the orbital indices. Seems to be needed for standard
 *                        W90 ordering.
 *
 * Example:
 *
 * .. sourcecode:: C
 *
 *  diverge_model_t* model = diverge_model_init();
 *  // set some model parameters...
 *  model->SU2 = 1;
 *  model->n_spin = 1;
 *  model->hop = diverge_read_W90_C( "/path/to/material_hr.dat",
 *                                  0, &model->n_hop, &model->n_orb );
 *  // do something with the hoppings here!
 */
rs_hopping_t* diverge_read_W90_C( const char* fname, index_t n_spin, index_t* len, index_t* n_orb );

#ifdef __cplusplus
} // extern "C"

/** alias of std::vector<:c:struct:`rs_hopping_t`> */
typedef vector<rs_hopping_t> vector_rs_hopping_t;

/**
 * .. sourcecode:: C++
 *
 *  vector<rs_hopping_t> diverge_read_W90( const char* fname,
 *      index_t n_spin, index_t* n_orb );
 *
 * C++ function corresponding to :c:func:`diverge_read_W90_C` that facilitates
 * the usage by returning an std::vector<:c:struct:`rs_hopping_t`> instead of messing
 * with pointers.
 */
vector_rs_hopping_t diverge_read_W90( const char* fname, index_t n_spin, index_t* n_orb );
#endif
