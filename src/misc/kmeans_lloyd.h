/* Copyright (C) 2024 Jonas B. Profe, Lennart Klebl; GPLv3+ (LICENSE.md) */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

int dkm_use_random_seed( void );
void dkm_set_use_random_seed( int use );

#ifdef __cplusplus
}
#endif
