/* Copyright (C) 2024 Jonas B. Profe, Lennart Klebl; GPLv3+ (LICENSE.md) */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/** returns license information as string */
const char* diverge_license( void );

/** prints out license information to stdout */
void diverge_license_print( void );

#ifdef __cplusplus
}
#endif
